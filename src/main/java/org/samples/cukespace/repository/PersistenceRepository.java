package org.samples.cukespace.repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.samples.cukespace.model.Identifiable;

public abstract class PersistenceRepository<T extends Identifiable> {

    @PersistenceContext
    private EntityManager entityManager;

    private Class<T> domainClass;

    public PersistenceRepository(Class<T> domainClass) {
        this.domainClass = domainClass;
    }

    public T findOne(Serializable id) {
        return entityManager.find(domainClass, id);
    }

    public boolean exists(Serializable id) {
        return (entityManager.find(domainClass, id) != null);
    }

    public long count() {
        return (Long) entityManager.createQuery("select count(*) from " + getDomainClassName())
                .getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        return entityManager.createQuery("from " + getDomainClassName())
                .getResultList();
    }

    public T save(T entity) {
        if (entity.getId() == null) {
            entityManager.persist(entity);
            return entity;
        } else {
            return entityManager.merge(entity);
        }
    }

    public void delete(T entity) {
        entityManager.remove(entityManager.merge(entity));
    }

    public void deleteAll() {
        entityManager.createQuery("delete " + getDomainClassName())
                .executeUpdate();
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @SuppressWarnings("unchecked")
    private Class<T> getDomainClass() {
        if (domainClass == null) {
            ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
            this.domainClass = (Class<T>) thisType.getActualTypeArguments()[0];
        }
        return domainClass;
    }

    private String getDomainClassName() {
        return getDomainClass().getName();
    }

}
