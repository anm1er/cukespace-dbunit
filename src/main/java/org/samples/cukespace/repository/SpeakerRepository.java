package org.samples.cukespace.repository;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.samples.cukespace.model.Speaker;
import org.samples.cukespace.model.Language;
import org.samples.arquillian.extensions.persistence.model.Speaker_;

public class SpeakerRepository extends PersistenceRepository<Speaker> {

    public SpeakerRepository() {
        super(Speaker.class);
    }

    public List<Speaker> findByLastNameLike(String namePattern) {
        CriteriaQuery<Speaker> speakerQuery = getEntityManager().getCriteriaBuilder()
                .createQuery(Speaker.class);
        Root<Speaker> fromSpeaker = speakerQuery.from(Speaker.class);
        speakerQuery.select(fromSpeaker).where(getEntityManager().getCriteriaBuilder()
                .like(fromSpeaker.get(Speaker_.lastName), namePattern));
        return getEntityManager().createQuery(speakerQuery).getResultList();
    }

    public List<Speaker> findByLanguage(Language language) {
        CriteriaQuery<Speaker> speakerQuery = getEntityManager().getCriteriaBuilder()
                .createQuery(Speaker.class);
        Root<Speaker> fromSpeaker = speakerQuery.from(Speaker.class);
        speakerQuery.select(fromSpeaker).where(getEntityManager().getCriteriaBuilder()
                .equal(fromSpeaker.get(Speaker_.talkLanguage), language));
        return getEntityManager().createQuery(speakerQuery).getResultList();
    }


}
