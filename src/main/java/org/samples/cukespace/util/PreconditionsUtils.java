package org.samples.cukespace.util;

public final class PreconditionsUtils {

    private PreconditionsUtils() {
    }

    public static <T> T checkNotNull(T reference, String propertyName) {
        if (reference == null) {
            throw new NullPointerException(propertyName + "can't be null");
        } else {
            return reference;
        }
    }

}
