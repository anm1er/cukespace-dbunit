package org.samples.cukespace.exception;

import javax.ejb.ApplicationException;

@SuppressWarnings("serial")
@ApplicationException(rollback = true)
public class SpeakerNotFoundException extends RuntimeException {

    public SpeakerNotFoundException() {
    }

    public SpeakerNotFoundException(String message) {
        super(message);
    }

    public SpeakerNotFoundException(Throwable cause) {
        super(cause);
    }

    public SpeakerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
