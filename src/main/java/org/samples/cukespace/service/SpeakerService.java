package org.samples.cukespace.service;

import java.util.List;

import org.samples.cukespace.model.Speaker;
import org.samples.cukespace.model.Language;

public interface SpeakerService {

    boolean speakerExists(Long id);

    long speakersCount();

    List<Speaker> getSpeakers();

    Speaker getSpeaker(Long id);

    Speaker saveSpeaker(Speaker speaker);

    void deleteSpeaker(Long id);

    void deleteSpeakers();

    List<Speaker> getSpeakersWithLastNameLike(String lastName);

    List<Speaker> getSpeakersWithLanguage(Language language);

}
