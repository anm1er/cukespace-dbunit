package org.samples.cukespace.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.samples.cukespace.exception.SpeakerNotFoundException;
import org.samples.cukespace.model.Language;
import org.samples.cukespace.model.Speaker;
import org.samples.cukespace.repository.SpeakerRepository;
import org.samples.cukespace.service.SpeakerService;
import org.samples.cukespace.util.PreconditionsUtils;

@Stateless
public class SpeakerServiceImpl implements SpeakerService {

    @Inject
    private SpeakerRepository repository;

    @Override
    public boolean speakerExists(Long id) {
        PreconditionsUtils.checkNotNull(id, "id");
        return repository.exists(id);
    }

    @Override
    public long speakersCount() {
        return repository.count();
    }

    @Override
    public List<Speaker> getSpeakers() {
        return repository.findAll();
    }

    @Override
    public Speaker getSpeaker(Long id) {
        PreconditionsUtils.checkNotNull(id, "id");
        Speaker speaker = repository.findOne(id);
        if (speaker == null) {
            throw new SpeakerNotFoundException("No speaker found with id: " + id);
        }
        return speaker;
    }

    @Override
    public Speaker saveSpeaker(Speaker speaker) {
        PreconditionsUtils.checkNotNull(speaker, "speaker");
        return repository.save(speaker);
    }

    @Override
    public void deleteSpeaker(Long id) {
        PreconditionsUtils.checkNotNull(id, "id");
        repository.delete(getSpeaker(id));
    }

    @Override
    public void deleteSpeakers() {
        repository.deleteAll();
    }

    @Override
    public List<Speaker> getSpeakersWithLastNameLike(String lastName) {
        return repository.findByLastNameLike(lastName);
    }

    @Override
    public List<Speaker> getSpeakersWithLanguage(Language language) {
        return repository.findByLanguage(language);
    }

}
