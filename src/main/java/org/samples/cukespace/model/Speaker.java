package org.samples.cukespace.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Conference speaker.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "speaker", uniqueConstraints = @UniqueConstraint(columnNames = {"firstName", "lastName"}))
public class Speaker extends BaseEntity {

    @NotNull
    @Size(min = 2, max = 20)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 20)
    private String lastName;

    @Size(min = 3, max = 30)
    private String organizationName;

    @NotNull
    @Size(min = 10, max = 100)
    private String talkTitle;

    @Enumerated(EnumType.ORDINAL)
    private Language talkLanguage;

    public Speaker() {
    }

    public Speaker(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getTalkTitle() {
        return talkTitle;
    }

    public void setTalkTitle(String talkTitle) {
        this.talkTitle = talkTitle;
    }

    public Language getTalkLanguage() {
        return talkLanguage;
    }

    public void setTalkLanguage(Language talkLanguage) {
        this.talkLanguage = talkLanguage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Speaker speaker = (Speaker) o;
        return Objects.equals(firstName, speaker.firstName)
                && Objects.equals(lastName, speaker.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Speaker{");
        sb.append("firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
