package org.samples.cukespace.model;

public enum Language {

    ENGLISH,
    FRENCH

}
