package org.samples.cukespace.model;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * A base domain object class with an id property provided.
 */
@SuppressWarnings("serial")
@MappedSuperclass
public abstract class BaseEntity implements Identifiable, Serializable {

    /**
     * The synthetic ID of the object.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Checks that entity has yet no persistent representation in the data store and no identifier value has been assigned.
     * @return {@code true} if identifier value has been assigned;
     * {@code false} otherwise.
     */
    public boolean isTransient() {
        return id == null;
    }

}
