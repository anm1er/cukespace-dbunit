package org.samples.cukespace.it;

import javax.inject.Inject;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.arquillian.ArquillianCucumber;
import cucumber.runtime.arquillian.api.Features;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.samples.cukespace.exception.SpeakerNotFoundException;
import org.samples.cukespace.it.util.DBUnitUtils;
import org.samples.cukespace.it.util.Deployments;
import org.samples.cukespace.it.util.SpeakerWorld;
import org.samples.cukespace.service.SpeakerService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(ArquillianCucumber.class)
@Features("features/remove-speakers.feature")
public class SpeakersRemovalFeatureTest {

    private SpeakerWorld world;

    @Inject
    private SpeakerService speakerService;

    @Deployment
    public static Archive<?> createDeployment() {
        return Deployments.getBaseDeployment()
                .addAsResource("datasets/speakers.yml", "speakers.yml");
    }

    @Before
    public void seedDatabase() {
        world = new SpeakerWorld();
        DBUnitUtils.usingDataset("speakers.yml");
    }

    @After
    public void eraseDatabase() {
        DBUnitUtils.deleteDataset("speakers.yml");
    }

    // ------------------------------------------------------------------------
    // Steps for 'Speaker removal by id' --------------------------------------
    // ------------------------------------------------------------------------

    @Given("^the speaker with id (\\d+) exists in the database$")
    public void speakerWithIdExists(Long id) {
        assertTrue(speakerService.speakerExists(id));
    }

    @When("^the speaker with id (\\d+) is removed$")
    public void speakerWithIdIsRemoved(Long id) {
        speakerService.deleteSpeaker(id);
    }

    @Then("^no speaker with id (\\d+) exists in the database$")
    public void noSpeakerWithIdExists(Long id) {
        assertFalse(speakerService.speakerExists(id));
    }

    // ------------------------------------------------------------------------
    // Steps for 'Exception is thrown' ----------------------------------------
    // ------------------------------------------------------------------------

    @Given("^the speaker with id (\\d+) does not exist in the database$")
    public void speakerWithIdDoesNotExist(Long id) {
        assertFalse(speakerService.speakerExists(id));
    }

    // This is the case when it is more natural to describe stimulus and expected response in a single unit:
    // I have to look into what is the best practice for this in the Cucumber-Cukespace world
    @When("^the non-existing speaker with id (\\d+) is removed$")
    public void nonExistingSpeakerWithIdIsRemoved(Long id) {
        try {
            speakerService.deleteSpeaker(id);
        } catch (RuntimeException e) {
            world.expectException(e);
        }
    }

    @Then("^SpeakerNotFoundException is thrown and no speaker with id (\\d+) is removed$")
    public void exceptionThrown(Long id) {
        assertThat(world.getExceptions()).isNotEmpty();
        assertThat(world.getExceptions().get(0))
                .isExactlyInstanceOf(SpeakerNotFoundException.class)
                .hasMessage("No speaker found with id: " + id);
    }

    // ------------------------------------------------------------------------
    // Steps for 'All speakers removal' ---------------------------------------
    // ------------------------------------------------------------------------

    @When("^all speakers are removed$")
    public void speakersAreRemoved() {
        speakerService.deleteSpeakers();
    }

    @Then("^no speakers exist in the database$")
    public void noSpeakersExist() {
        assertEquals(0, speakerService.speakersCount());
    }

}
