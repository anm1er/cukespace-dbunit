package org.samples.cukespace.it;

import javax.inject.Inject;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.arquillian.ArquillianCucumber;
import cucumber.runtime.arquillian.api.Features;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.samples.cukespace.exception.SpeakerNotFoundException;
import org.samples.cukespace.it.util.DBUnitUtils;
import org.samples.cukespace.it.util.Deployments;
import org.samples.cukespace.it.util.SpeakerWorld;
import org.samples.cukespace.model.Language;
import org.samples.cukespace.service.SpeakerService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(ArquillianCucumber.class)
@Features("features/search-speakers.feature")
public class SpeakersSearchFeatureTest {

    private SpeakerWorld world;

    @Inject
    private SpeakerService speakerService;

    @Deployment
    public static Archive<?> createDeployment() {
        return Deployments.getBaseDeployment()
                .addAsResource("datasets/speakers.yml", "speakers.yml");
    }

    @Before
    public void seedDatabase() {
        world = new SpeakerWorld();
        DBUnitUtils.usingDataset("speakers.yml");
    }

    @After
    public void eraseDatabase() {
        DBUnitUtils.deleteDataset("speakers.yml");
    }

    // ------------------------------------------------------------------------
    // Steps for 'Retrieval of a speaker by id' -------------------------------
    // ------------------------------------------------------------------------

    @Given("^the searched speaker with id (\\d+) exists in the database$")
    public void speakerWithIdExists(Long id) {
        assertTrue(speakerService.speakerExists(id));
    }

    @When("^the speaker with id (\\d+) is searched$")
    public void speakerWithIdIsSearched(Long id) {
        world.setResult(speakerService.getSpeaker(id));
    }

    @Then("^single speaker should be retrieved$")
    public void singleSpeakerIsRetrieved() {
        assertNotNull(world.getSingleResult());
    }

    // ------------------------------------------------------------------------
    // Steps for 'Retrieval of a speaker by last name pattern' ----------------
    // ------------------------------------------------------------------------

    @When("^the speakers with last name pattern \"([^\"]*)\" are searched$")
    public void speakersWithLastNamePatternAreSearched(String pattern) {
        world.setResult(speakerService.getSpeakersWithLastNameLike(pattern));
    }

    @Then("^(\\d+) speakers with the last name pattern should be retrieved$")
    public void speakersWithLastNamePatternAreRetrieved(int speakersNumber) {
        assertNotNull(world.getResultList());
        assertEquals(speakersNumber, world.getResultList().size());
    }

    // ------------------------------------------------------------------------
    // Steps for 'Retrieval of a speaker by talk language' ----------------
    // ------------------------------------------------------------------------

    @When("^the speakers with language \"([^\"]*)\" are searched$")
    public void speakersWithLanguageAreSearched(String language) {
        world.setResult(speakerService.getSpeakersWithLanguage(Language.valueOf(language)));
    }

    @Then("^(\\d+) speakers with the language should be retrieved$")
    public void speakersWithLanguageAreRetrieved(int speakersNumber) {
        assertNotNull(world.getResultList());
        assertEquals(speakersNumber, world.getResultList().size());
    }

    // ------------------------------------------------------------------------
    // Steps for 'Exception is thrown' ----------------------------------------
    // ------------------------------------------------------------------------

    @Given("^the searched speaker with id (\\d+) does not exist in the database$")
    public void speakerWithIdDoesNotExist(Long id) {
        assertFalse(speakerService.speakerExists(id));
    }

    @When("^the non-existing searched speaker with id (\\d+) is searched$")
    public void nonExistingSpeakerWithIdIsSearched(Long id) {
        try {
            speakerService.deleteSpeaker(id);
        } catch (RuntimeException e) {
            world.expectException(e);
        }
    }

    @Then("^SpeakerNotFoundException is thrown and no speaker with id (\\d+) is retrieved$")
    public void exceptionThrown(Long id) {
        assertThat(world.getExceptions()).isNotEmpty();
        assertThat(world.getExceptions().get(0))
                .isExactlyInstanceOf(SpeakerNotFoundException.class)
                .hasMessage("No speaker found with id: " + id);
    }

    // ------------------------------------------------------------------------
    // Steps for 'All speakers retrieval' -------------------------------------
    // ------------------------------------------------------------------------

    @Given("^the database is populated with (\\d+) speakers$")
    public void theDatabaseIsPopulatedWithNSpeakers(int count) {
        assertEquals(count, speakerService.speakersCount());
    }

    @When("^all speakers are searched$")
    public void speakersAreSearched() {
        world.setResult(speakerService.getSpeakers());
    }

    @SuppressWarnings("unchecked")
    @Then("^the number of the retrieved speakers should be (\\d+)$")
    public void allSpeakersAreRetrieved(int speakersNumber) {
        assertEquals(speakersNumber, world.getResultList().size());
    }

}
