package org.samples.cukespace.it;

import javax.inject.Inject;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.arquillian.ArquillianCucumber;
import cucumber.runtime.arquillian.api.Features;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.samples.cukespace.it.util.DBUnitUtils;
import org.samples.cukespace.it.util.Deployments;
import org.samples.cukespace.it.util.SpeakerWorld;
import org.samples.cukespace.model.Language;
import org.samples.cukespace.model.Speaker;
import org.samples.cukespace.service.SpeakerService;

import static org.junit.Assert.assertEquals;

@RunWith(ArquillianCucumber.class)
@Features("features/update-speakers.feature")
public class SpeakersUpdateFeatureTest {

    private SpeakerWorld world;

    @Inject
    private SpeakerService speakerService;

    @Deployment
    public static Archive<?> createDeployment() {
        return Deployments.getBaseDeployment()
                .addAsResource("datasets/speakers.yml", "speakers.yml");
    }

    @Before
    public void seedDatabase() {
        world = new SpeakerWorld();
        DBUnitUtils.usingDataset("speakers.yml");
    }

    @After
    public void eraseDatabase() {
        DBUnitUtils.deleteDataset("speakers.yml");
    }

    // ------------------------------------------------------------------------
    // Steps for 'Update of a speaker's talk language' --------------------------------------------
    // ------------------------------------------------------------------------

    @Given("^speaker with id (\\d+) has been retrieved from the database$")
    public void speakerWithIsRetrieved(Long id) {
        world.setResult(speakerService.getSpeaker(id));
    }

    @Given("^the speaker has talk language \"([^\"]*)\"$")
    public void speakerHasLanguage(String language) {
        assertEquals(language, world.getSingleResult().getTalkLanguage().toString());
    }

    @When("^the speaker's language is updated to \"([^\"]*)\"$")
    public void speakerIsUpdated(String newLanguage) {
        Speaker speaker = world.getSingleResult();
        speaker.setTalkLanguage(Language.valueOf(newLanguage));
        speakerService.saveSpeaker(speaker);
    }

    @Then("^searching speakers by talk language \"([^\"]*)\" should return (\\d+) records$")
    public void speakersWithLanguageAreRetrieved(String language, int number) {
        assertEquals(number, speakerService.getSpeakersWithLanguage(Language.valueOf(language)).size());
    }

}
