package org.samples.cukespace.it.util;

import java.util.ArrayList;
import java.util.List;

/**
 * In Ruby all Step Definitions will run in the context of the current World instance
 * (a new instance created for each scenario).
 * The purpose of a "World" is twofold:
 * <p/>
 * 1) Isolate state between scenarios.
 * <p/>
 * 2) Share data between step definitions within a scenario.
 */
public class World<T> {

    private List<RuntimeException> exceptions = new ArrayList<>();
    private T singleResult;
    private List<T> resultList;

    public void expectException(RuntimeException exception) {
        exceptions.add(exception);
    }

    public T getSingleResult() {
        return singleResult;
    }

    public void setResult(T singleResult) {
        this.singleResult = singleResult;
    }

    public List<RuntimeException> getExceptions() {
        return exceptions;
    }

    public List<T> getResultList() {
        return resultList;
    }

    public void setResult(List<T> resultList) {
        this.resultList = resultList;
    }

}
