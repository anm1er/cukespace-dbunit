package org.samples.cukespace.it.util;

import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.operation.DatabaseOperation;
import org.jboss.arquillian.persistence.dbunit.dataset.yaml.YamlDataSet;

/**
 * This utility class is a quick and dirty workaround for cukespace issue 37 seeding database in cukespace tests
 * with arquillian-persistence extension suggested by Rafael Pestano.
 */
public final class DBUnitUtils {

    public static final String JNDI_DATASOURCE_NAME = "java:/jdbc/cukespace-samples";

    private DBUnitUtils() {
    }

    private static DataSource ds;

    private static DatabaseConnection databaseConnection;

    public static void usingDataset(String datasetFile) {
        datasetFile = getCheckedDatasetFileName(datasetFile);
        try {
            initConnection();
            DatabaseOperation.CLEAN_INSERT.execute(databaseConnection,
                    new YamlDataSet(DBUnitUtils.class.getResourceAsStream(datasetFile)));
        } catch (Exception e) {
            throw new RuntimeException("Error seeding database: " + e.getMessage());
        } finally {
            closeConnection();
        }
    }

    public static void deleteDataset(String datasetFile) {
        datasetFile = getCheckedDatasetFileName(datasetFile);
        try {
            initConnection();
            DatabaseOperation.DELETE_ALL.execute(databaseConnection,
                    new YamlDataSet(DBUnitUtils.class.getResourceAsStream(datasetFile)));
        } catch (Exception e) {
            throw new RuntimeException("Error erasing database : " + e.getMessage());
        } finally {
            closeConnection();
        }
    }

    private static void initConnection() throws SQLException, NamingException, DatabaseUnitException {
        if (ds == null) {
            ds = (DataSource) new InitialContext().lookup(JNDI_DATASOURCE_NAME);
        }
        databaseConnection = new DatabaseConnection(ds.getConnection());
    }

    private static void closeConnection() {
        try {
            if (databaseConnection != null && !databaseConnection.getConnection().isClosed()) {
                databaseConnection.getConnection().close();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error closing connection: " + e.getMessage());
        }
    }

    private static String getCheckedDatasetFileName(String datasetFile) {
        // todo: allow for dataset formats other than YAML to be accepted
        if (!datasetFile.endsWith(".yml")) {
            throw new IllegalArgumentException("Not supported dataset file format");
        }
        if (!datasetFile.startsWith("/")) {
            datasetFile = "/" + datasetFile;
        }
        return datasetFile;
    }

}
