package org.samples.cukespace.it.util;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.samples.cukespace.exception.SpeakerNotFoundException;
import org.samples.cukespace.model.Speaker;
import org.samples.cukespace.repository.SpeakerRepository;
import org.samples.cukespace.service.SpeakerService;
import org.samples.cukespace.service.impl.SpeakerServiceImpl;
import org.samples.cukespace.util.PreconditionsUtils;

public final class Deployments {

    public static final String WEB_ARCHIVE_NAME = "cukespace-with-dbunit.war";
    public static final String DATASOURCE_FILE = "test-ds.xml";

    private Deployments() {
    }

    public static WebArchive getBaseDeployment() {
        return ShrinkWrap.create(WebArchive.class, WEB_ARCHIVE_NAME)
                .addPackage(Speaker.class.getPackage())
                .addPackage(SpeakerRepository.class.getPackage())
                .addClass(SpeakerService.class)
                .addClass(SpeakerServiceImpl.class)
                .addClass(SpeakerNotFoundException.class)
                .addClass(PreconditionsUtils.class)
                // Adds utilities for tests
                .addPackage(DBUnitUtils.class.getPackage())
                .addPackages(true, "org.assertj")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(DATASOURCE_FILE);
    }


}
