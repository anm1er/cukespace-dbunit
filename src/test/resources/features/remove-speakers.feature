Feature: Remove speakers

  Scenario: Speaker removal by id
    To allow to remove a speaker stored in the database by id
    Given the speaker with id 1 exists in the database
    When the speaker with id 1 is removed
    Then no speaker with id 1 exists in the database

  Scenario: Exception is thrown when trying to remove speaker not stored in the database
    To throw exception if no speaker with the specified id exists in the database
    Given the speaker with id 10 does not exist in the database
    When the non-existing speaker with id 10 is removed
    Then SpeakerNotFoundException is thrown and no speaker with id 10 is removed

  Scenario: All speakers removal
    To allow to remove all speakers stored in the database
    When all speakers are removed
    Then no speakers exist in the database
