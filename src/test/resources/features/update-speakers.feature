Feature: Update speakers

  Scenario Outline: Update of a speaker's talk language
    To allow to update a speaker's talk language
    Given speaker with id <id> has been retrieved from the database
    And the speaker has talk language "<old>"
    When the speaker's language is updated to "<new>"
    Then searching speakers by talk language "<new>" should return <number> records
  Examples:
  | id   | old     | new      | number |
  | 1    | ENGLISH |  FRENCH  |   1    |

