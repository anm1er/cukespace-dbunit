Feature: Search speakers

  Scenario: Retrieval of a speaker by id
  To allow to retrieve speaker stored in the database by id
    Given the searched speaker with id 1 exists in the database
    When the speaker with id 1 is searched
    Then single speaker should be retrieved

  Scenario: Retrieval of speakers by last name pattern
  To allow to retrieve speakers stored in the database by last name pattern
    When the speakers with last name pattern "%d" are searched
    Then 3 speakers with the last name pattern should be retrieved

  Scenario Outline: Retrieval of speakers by talk language
  To allow to retrieve speakers stored in the database by talk language
    When the speakers with language "<language>" are searched
    Then <number> speakers with the language should be retrieved
  Examples:
  | language     | number |
  | ENGLISH      | 5      |
  | FRENCH       | 0      |

  Scenario: Exception is thrown when trying to retrieve speaker not stored in the database
  To throw exception if no speaker with the specified id exists in the database
    Given the searched speaker with id 10 does not exist in the database
    When the non-existing searched speaker with id 10 is searched
    Then SpeakerNotFoundException is thrown and no speaker with id 10 is retrieved

  Scenario: All speakers retrieval
  To allow to retrieve all speakers stored in the database
    Given the database is populated with 5 speakers
    When all speakers are searched
    Then the number of the retrieved speakers should be 5
