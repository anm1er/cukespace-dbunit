# Simple Cukespace Samples to Test Persistence Layer

[Cukespace] is a project that allows running and deployment of **Cucumber** features in the AS of your choice using the **Arquillian**
test framework.

### Note:

Because of issue https://github.com/cukespace/cukespace/issues/37 seeding database with arquillian-persistence extension
which uses **DBUnit** behind the scenes does not work with cucumber test methods (cucumber test life cycle is different from that of
junit).

[Cukespace]: https://github.com/cukespace/cukespace